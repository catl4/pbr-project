(deftemplate Part
	(slot id)
	(slot index)
	(slot part_index)
	(multislot measure)
)

(deftemplate Attributes
	(slot divisions)
	(slot clef_index)
	(slot index)
	(slot part_index)
	(slot time_index)
	(slot key_index)
)

(deftemplate Measure
	(slot barline_index)
	(slot width)
	(slot direction_index)
	(slot index)
	(slot number)
	(slot part_index)
	(slot attributes_index)
	(multislot note)
)

(deftemplate Note
	(slot dynamics)
	(slot index)
	(slot default_y)
	(slot duration)
	(slot lyric_index)
	(slot stem)
	(slot type)
	(slot part_index)
	(slot beam_index)
	(slot pitch_index)
	(slot default_x)
	(slot voice)
	(multislot beam)
)

(deftemplate Pitch
	(slot step)
	(slot octave)
	(slot index)
	(slot part_index)
)

(deftemplate Beam
	(slot number)
	(slot index)
	(slot data)
	(slot part_index)
)

(deftemplate Key
	(slot fifths)
	(slot index)
	(slot part_index)
)

(deftemplate Time
	(slot beat_type)
	(slot index)
	(slot beats)
	(slot part_index)
)

(deftemplate Transpose
)

(deftemplate Direction
	(slot index)
	(slot placement)
	(slot part_index)
	(slot direction_type_index)
	(slot sound_index)
)

(deftemplate Direction_Type
	(slot metronome_index)
	(slot index)
	(slot part_index)
)

(deftemplate Metronome
	(slot per_minute)
	(slot index)
	(slot default_x)
	(slot part_index)
	(slot parentheses)
	(slot relative_y)
	(slot beat_unit)
)

(deftemplate Sound
	(slot tempo)
	(slot index)
	(slot part_index)
)

(deftemplate Clef
	(slot index)
	(slot line)
	(slot part_index)
	(slot sign)
	(slot clef_octave_change)
)

(deftemplate Barline
	(slot bar_style)
	(slot location)
	(slot index)
	(slot part_index)
)

(deftemplate Tie
)

(deftemplate Instrument
)

(deftemplate Print
)

(deftemplate Notations
)

(deftemplate Backup
)

(deftemplate Unpitched
	(slot display_step)
	(slot index)
	(slot part_index)
)

(deftemplate Articulations
)

(deftemplate Repeat
)

(deftemplate Staff_layout
)

(deftemplate Tied
)

(deftemplate System_layout
)

(deftemplate System_margins
)

(deffacts parts
	(Part (index 0)(id P1)(measure 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14)(part_index 1))
)

(deffacts attributess
	(Attributes (index 0)(divisions 4)(key_index 0)(time_index 0)(clef_index 0)(part_index 1))
)

(deffacts measures
	(Measure (index 0)(number 1)(width 164.04)(attributes_index 0)(direction_index 0)(note 0 1 2 3)(part_index 1))
	(Measure (index 1)(number 2)(width 69.14)(note 4 5)(part_index 1))
	(Measure (index 2)(number 3)(width 129.23)(note 6 7 8 9 10 11)(part_index 1))
	(Measure (index 3)(number 4)(width 94.77)(note 12 13 14 15)(part_index 1))
	(Measure (index 4)(number 5)(width 77.54)(note 16 17 18)(part_index 1))
	(Measure (index 5)(number 6)(width 129.23)(note 19 20 21 22 23 24)(part_index 1))
	(Measure (index 6)(number 7)(width 77.54)(note 25 26 27)(part_index 1))
	(Measure (index 7)(number 8)(width 129.23)(note 28 29 30 31 32 33)(part_index 1))
	(Measure (index 8)(number 9)(width 77.54)(note 34 35 36)(part_index 1))
	(Measure (index 9)(number 10)(width 129.23)(note 37 38 39 40 41 42)(part_index 1))
	(Measure (index 10)(number 11)(width 241.77)(note 43 44 45)(part_index 1))
	(Measure (index 11)(number 12)(width 211.03)(note 46 47 48 49)(part_index 1))
	(Measure (index 12)(number 13)(width 219.66)(note 50 51 52 53)(part_index 1))
	(Measure (index 13)(number 14)(width 219.66)(note 54 55 56 57)(part_index 1))
	(Measure (index 14)(number 15)(width 185.37)(note 58 59)(barline_index 0)(part_index 1))
)

(deffacts notes
	(Note (index 0)(default_x 79.27)(default_y -15.00)(dynamics 100.00)(pitch_index 0)(duration 2)(voice 1)(type eighth)(stem down)(beam_index 0)(lyric_index 719)(part_index 1))
	(Note (index 1)(default_x 99.27)(default_y -10.00)(dynamics 100.00)(pitch_index 1)(duration 2)(voice 1)(type eighth)(stem down)(beam_index 1)(part_index 1))
	(Note (index 2)(default_x 119.27)(default_y -10.00)(dynamics 100.00)(pitch_index 2)(duration 2)(voice 1)(type eighth)(stem down)(beam_index 2)(part_index 1))
	(Note (index 3)(default_x 139.27)(default_y -10.00)(dynamics 100.00)(pitch_index 3)(duration 2)(voice 1)(type eighth)(stem down)(beam_index 3)(part_index 1))
	(Note (index 4)(default_x 10.00)(default_y -10.00)(dynamics 100.00)(pitch_index 4)(duration 6)(voice 1)(type quarter)(stem down)(part_index 1))
	(Note (index 5)(default_x 44.37)(default_y -5.00)(dynamics 100.00)(pitch_index 5)(duration 2)(voice 1)(type eighth)(stem down)(part_index 1))
	(Note (index 6)(default_x 10.00)(default_y -15.00)(dynamics 100.00)(pitch_index 6)(duration 1)(voice 1)(type 16th)(stem down)(beam 4 5)(part_index 1))
	(Note (index 7)(default_x 26.34)(default_y -15.00)(dynamics 100.00)(pitch_index 7)(duration 1)(voice 1)(type 16th)(stem down)(beam 6 7)(part_index 1))
	(Note (index 8)(default_x 42.68)(default_y -15.00)(dynamics 100.00)(pitch_index 8)(duration 1)(voice 1)(type 16th)(stem down)(beam 8 9)(part_index 1))
	(Note (index 9)(default_x 59.01)(default_y -15.00)(dynamics 100.00)(pitch_index 9)(duration 1)(voice 1)(type 16th)(stem down)(beam 10 11)(part_index 1))
	(Note (index 10)(default_x 75.35)(default_y -25.00)(dynamics 100.00)(pitch_index 10)(duration 2)(voice 1)(type eighth)(stem down)(beam_index 12)(part_index 1))
	(Note (index 11)(default_x 101.49)(default_y -15.00)(dynamics 100.00)(pitch_index 11)(duration 2)(voice 1)(type eighth)(stem down)(beam_index 13)(part_index 1))
	(Note (index 12)(default_x 10.00)(default_y -10.00)(dynamics 100.00)(pitch_index 12)(duration 2)(voice 1)(type eighth)(stem down)(beam_index 14)(part_index 1))
	(Note (index 13)(default_x 30.00)(default_y -5.00)(dynamics 100.00)(pitch_index 13)(duration 2)(voice 1)(type eighth)(stem down)(beam_index 15)(part_index 1))
	(Note (index 14)(default_x 50.00)(default_y -15.00)(dynamics 100.00)(pitch_index 14)(duration 2)(voice 1)(type eighth)(stem down)(beam_index 16)(part_index 1))
	(Note (index 15)(default_x 70.00)(default_y -25.00)(dynamics 100.00)(pitch_index 15)(duration 2)(voice 1)(type eighth)(stem down)(beam_index 17)(part_index 1))
	(Note (index 16)(default_x 10.00)(default_y -30.00)(dynamics 100.00)(pitch_index 16)(duration 2)(voice 1)(type eighth)(stem up)(beam_index 18)(part_index 1))
	(Note (index 17)(default_x 28.32)(default_y -30.00)(dynamics 100.00)(pitch_index 17)(duration 2)(voice 1)(type eighth)(stem up)(beam_index 19)(part_index 1))
	(Note (index 18)(default_x 46.63)(default_y -30.00)(dynamics 100.00)(pitch_index 18)(duration 4)(voice 1)(type quarter)(stem up)(part_index 1))
	(Note (index 19)(default_x 10.00)(default_y -15.00)(dynamics 100.00)(pitch_index 19)(duration 2)(voice 1)(type eighth)(stem down)(beam_index 20)(part_index 1))
	(Note (index 20)(default_x 33.74)(default_y -15.00)(dynamics 100.00)(pitch_index 20)(duration 1)(voice 1)(type 16th)(stem down)(beam 21 22)(part_index 1))
	(Note (index 21)(default_x 49.40)(default_y -15.00)(dynamics 100.00)(pitch_index 21)(duration 1)(voice 1)(type 16th)(stem down)(beam 23 24)(part_index 1))
	(Note (index 22)(default_x 65.07)(default_y -25.00)(dynamics 100.00)(pitch_index 22)(duration 2)(voice 1)(type eighth)(stem up)(beam_index 25)(part_index 1))
	(Note (index 23)(default_x 88.80)(default_y -25.00)(dynamics 100.00)(pitch_index 23)(duration 1)(voice 1)(type 16th)(stem up)(beam 26 27)(part_index 1))
	(Note (index 24)(default_x 104.47)(default_y -35.00)(dynamics 100.00)(pitch_index 24)(duration 1)(voice 1)(type 16th)(stem up)(beam 28 29)(part_index 1))
	(Note (index 25)(default_x 10.00)(default_y -30.00)(dynamics 100.00)(pitch_index 25)(duration 2)(voice 1)(type eighth)(stem up)(beam_index 30)(part_index 1))
	(Note (index 26)(default_x 28.32)(default_y -30.00)(dynamics 100.00)(pitch_index 26)(duration 2)(voice 1)(type eighth)(stem up)(beam_index 31)(part_index 1))
	(Note (index 27)(default_x 46.63)(default_y -30.00)(dynamics 100.00)(pitch_index 27)(duration 4)(voice 1)(type quarter)(stem up)(part_index 1))
	(Note (index 28)(default_x 10.00)(default_y -15.00)(dynamics 100.00)(pitch_index 28)(duration 2)(voice 1)(type eighth)(stem down)(beam_index 32)(part_index 1))
	(Note (index 29)(default_x 33.74)(default_y -15.00)(dynamics 100.00)(pitch_index 29)(duration 1)(voice 1)(type 16th)(stem down)(beam 33 34)(part_index 1))
	(Note (index 30)(default_x 49.40)(default_y -15.00)(dynamics 100.00)(pitch_index 30)(duration 1)(voice 1)(type 16th)(stem down)(beam 35 36)(part_index 1))
	(Note (index 31)(default_x 65.07)(default_y -25.00)(dynamics 100.00)(pitch_index 31)(duration 2)(voice 1)(type eighth)(stem up)(beam_index 37)(part_index 1))
	(Note (index 32)(default_x 88.80)(default_y -25.00)(dynamics 100.00)(pitch_index 32)(duration 1)(voice 1)(type 16th)(stem up)(beam 38 39)(part_index 1))
	(Note (index 33)(default_x 104.47)(default_y -35.00)(dynamics 100.00)(pitch_index 33)(duration 1)(voice 1)(type 16th)(stem up)(beam 40 41)(part_index 1))
	(Note (index 34)(default_x 10.00)(default_y -30.00)(dynamics 100.00)(pitch_index 34)(duration 2)(voice 1)(type eighth)(stem up)(beam_index 42)(part_index 1))
	(Note (index 35)(default_x 28.32)(default_y -30.00)(dynamics 100.00)(pitch_index 35)(duration 2)(voice 1)(type eighth)(stem up)(beam_index 43)(part_index 1))
	(Note (index 36)(default_x 46.63)(default_y -30.00)(dynamics 100.00)(pitch_index 36)(duration 4)(voice 1)(type quarter)(stem up)(part_index 1))
	(Note (index 37)(default_x 10.00)(default_y -15.00)(dynamics 100.00)(pitch_index 37)(duration 2)(voice 1)(type eighth)(stem down)(beam_index 44)(part_index 1))
	(Note (index 38)(default_x 33.74)(default_y -15.00)(dynamics 100.00)(pitch_index 38)(duration 1)(voice 1)(type 16th)(stem down)(beam 45 46)(part_index 1))
	(Note (index 39)(default_x 49.40)(default_y -15.00)(dynamics 100.00)(pitch_index 39)(duration 1)(voice 1)(type 16th)(stem down)(beam 47 48)(part_index 1))
	(Note (index 40)(default_x 65.07)(default_y -25.00)(dynamics 100.00)(pitch_index 40)(duration 2)(voice 1)(type eighth)(stem up)(beam_index 49)(part_index 1))
	(Note (index 41)(default_x 88.80)(default_y -25.00)(dynamics 100.00)(pitch_index 41)(duration 1)(voice 1)(type 16th)(stem up)(beam 50 51)(part_index 1))
	(Note (index 42)(default_x 104.47)(default_y -35.00)(dynamics 100.00)(pitch_index 42)(duration 1)(voice 1)(type 16th)(stem up)(beam 52 53)(part_index 1))
	(Note (index 43)(default_x 58.17)(default_y -30.00)(dynamics 100.00)(pitch_index 43)(duration 2)(voice 1)(type eighth)(stem up)(beam_index 54)(part_index 1))
	(Note (index 44)(default_x 108.73)(default_y -30.00)(dynamics 100.00)(pitch_index 44)(duration 2)(voice 1)(type eighth)(stem up)(beam_index 55)(part_index 1))
	(Note (index 45)(default_x 159.28)(default_y -30.00)(dynamics 100.00)(pitch_index 45)(duration 4)(voice 1)(type quarter)(stem up)(part_index 1))
	(Note (index 46)(duration 2)(voice 1)(type eighth)(part_index 1))
	(Note (index 47)(default_x 60.01)(default_y -15.00)(dynamics 100.00)(pitch_index 46)(duration 2)(voice 1)(type eighth)(stem down)(part_index 1))
	(Note (index 48)(default_x 109.81)(default_y -10.00)(dynamics 100.00)(pitch_index 47)(duration 2)(voice 1)(type eighth)(stem down)(beam_index 56)(part_index 1))
	(Note (index 49)(default_x 159.62)(default_y -10.00)(dynamics 100.00)(pitch_index 48)(duration 2)(voice 1)(type eighth)(stem down)(beam_index 57)(part_index 1))
	(Note (index 50)(default_x 10.00)(default_y -15.00)(dynamics 100.00)(pitch_index 49)(duration 3)(voice 1)(type eighth)(stem down)(beam_index 58)(part_index 1))
	(Note (index 51)(default_x 75.99)(default_y -10.00)(dynamics 100.00)(pitch_index 50)(duration 1)(voice 1)(type 16th)(stem down)(beam 59 60)(part_index 1))
	(Note (index 52)(default_x 109.82)(default_y -25.00)(dynamics 100.00)(pitch_index 51)(duration 2)(voice 1)(type eighth)(stem up)(beam_index 61)(part_index 1))
	(Note (index 53)(default_x 163.94)(default_y -25.00)(dynamics 100.00)(pitch_index 52)(duration 2)(voice 1)(type eighth)(stem up)(beam_index 62)(part_index 1))
	(Note (index 54)(default_x 10.00)(default_y -25.00)(dynamics 100.00)(pitch_index 53)(duration 2)(voice 1)(type eighth)(stem up)(beam_index 63)(part_index 1))
	(Note (index 55)(default_x 64.12)(default_y -50.00)(dynamics 100.00)(pitch_index 54)(duration 2)(voice 1)(type eighth)(stem up)(beam_index 64)(part_index 1))
	(Note (index 56)(default_x 118.24)(default_y -25.00)(dynamics 100.00)(pitch_index 55)(duration 3)(voice 1)(type eighth)(stem up)(beam_index 65)(part_index 1))
	(Note (index 57)(default_x 184.24)(default_y -45.00)(dynamics 100.00)(pitch_index 56)(duration 1)(voice 1)(type 16th)(stem up)(beam 66 67)(part_index 1))
	(Note (index 58)(default_x 10.00)(default_y -30.00)(dynamics 100.00)(pitch_index 57)(duration 4)(voice 1)(type quarter)(stem up)(part_index 1))
	(Note (index 59)(default_x 92.38)(default_y -30.00)(dynamics 100.00)(pitch_index 58)(duration 4)(voice 1)(type quarter)(stem up)(part_index 1))
)

(deffacts pitchs
	(Pitch (index 0)(step C)(octave 4)(part_index 1))
	(Pitch (index 1)(step D)(octave 4)(part_index 1))
	(Pitch (index 2)(step D)(octave 4)(part_index 1))
	(Pitch (index 3)(step D)(octave 4)(part_index 1))
	(Pitch (index 4)(step D)(octave 4)(part_index 1))
	(Pitch (index 5)(step E)(octave 4)(part_index 1))
	(Pitch (index 6)(step C)(octave 4)(part_index 1))
	(Pitch (index 7)(step C)(octave 4)(part_index 1))
	(Pitch (index 8)(step C)(octave 4)(part_index 1))
	(Pitch (index 9)(step C)(octave 4)(part_index 1))
	(Pitch (index 10)(step A)(octave 3)(part_index 1))
	(Pitch (index 11)(step C)(octave 4)(part_index 1))
	(Pitch (index 12)(step D)(octave 4)(part_index 1))
	(Pitch (index 13)(step E)(octave 4)(part_index 1))
	(Pitch (index 14)(step C)(octave 4)(part_index 1))
	(Pitch (index 15)(step A)(octave 3)(part_index 1))
	(Pitch (index 16)(step G)(octave 3)(part_index 1))
	(Pitch (index 17)(step G)(octave 3)(part_index 1))
	(Pitch (index 18)(step G)(octave 3)(part_index 1))
	(Pitch (index 19)(step C)(octave 4)(part_index 1))
	(Pitch (index 20)(step C)(octave 4)(part_index 1))
	(Pitch (index 21)(step C)(octave 4)(part_index 1))
	(Pitch (index 22)(step A)(octave 3)(part_index 1))
	(Pitch (index 23)(step A)(octave 3)(part_index 1))
	(Pitch (index 24)(step F)(octave 3)(part_index 1))
	(Pitch (index 25)(step G)(octave 3)(part_index 1))
	(Pitch (index 26)(step G)(octave 3)(part_index 1))
	(Pitch (index 27)(step G)(octave 3)(part_index 1))
	(Pitch (index 28)(step C)(octave 4)(part_index 1))
	(Pitch (index 29)(step C)(octave 4)(part_index 1))
	(Pitch (index 30)(step C)(octave 4)(part_index 1))
	(Pitch (index 31)(step A)(octave 3)(part_index 1))
	(Pitch (index 32)(step A)(octave 3)(part_index 1))
	(Pitch (index 33)(step F)(octave 3)(part_index 1))
	(Pitch (index 34)(step G)(octave 3)(part_index 1))
	(Pitch (index 35)(step G)(octave 3)(part_index 1))
	(Pitch (index 36)(step G)(octave 3)(part_index 1))
	(Pitch (index 37)(step C)(octave 4)(part_index 1))
	(Pitch (index 38)(step C)(octave 4)(part_index 1))
	(Pitch (index 39)(step C)(octave 4)(part_index 1))
	(Pitch (index 40)(step A)(octave 3)(part_index 1))
	(Pitch (index 41)(step A)(octave 3)(part_index 1))
	(Pitch (index 42)(step F)(octave 3)(part_index 1))
	(Pitch (index 43)(step G)(octave 3)(part_index 1))
	(Pitch (index 44)(step G)(octave 3)(part_index 1))
	(Pitch (index 45)(step G)(octave 3)(part_index 1))
	(Pitch (index 46)(step C)(octave 4)(part_index 1))
	(Pitch (index 47)(step D)(octave 4)(part_index 1))
	(Pitch (index 48)(step D)(octave 4)(part_index 1))
	(Pitch (index 49)(step C)(octave 4)(part_index 1))
	(Pitch (index 50)(step D)(octave 4)(part_index 1))
	(Pitch (index 51)(step A)(octave 3)(part_index 1))
	(Pitch (index 52)(step A)(octave 3)(part_index 1))
	(Pitch (index 53)(step A)(octave 3)(part_index 1))
	(Pitch (index 54)(step C)(octave 3)(part_index 1))
	(Pitch (index 55)(step A)(octave 3)(part_index 1))
	(Pitch (index 56)(step D)(octave 3)(part_index 1))
	(Pitch (index 57)(step G)(octave 3)(part_index 1))
	(Pitch (index 58)(step G)(octave 3)(part_index 1))
)

(deffacts beams
	(Beam (index 0)(data begin)(number 1)(part_index 1))
	(Beam (index 1)(data end)(number 1)(part_index 1))
	(Beam (index 2)(data begin)(number 1)(part_index 1))
	(Beam (index 3)(data end)(number 1)(part_index 1))
	(Beam (index 4)(data begin)(number 1)(part_index 1))
	(Beam (index 5)(data begin)(number 2)(part_index 1))
	(Beam (index 6)(data continue)(number 1)(part_index 1))
	(Beam (index 7)(data continue)(number 2)(part_index 1))
	(Beam (index 8)(data continue)(number 1)(part_index 1))
	(Beam (index 9)(data continue)(number 2)(part_index 1))
	(Beam (index 10)(data end)(number 1)(part_index 1))
	(Beam (index 11)(data end)(number 2)(part_index 1))
	(Beam (index 12)(data begin)(number 1)(part_index 1))
	(Beam (index 13)(data end)(number 1)(part_index 1))
	(Beam (index 14)(data begin)(number 1)(part_index 1))
	(Beam (index 15)(data end)(number 1)(part_index 1))
	(Beam (index 16)(data begin)(number 1)(part_index 1))
	(Beam (index 17)(data end)(number 1)(part_index 1))
	(Beam (index 18)(data begin)(number 1)(part_index 1))
	(Beam (index 19)(data end)(number 1)(part_index 1))
	(Beam (index 20)(data begin)(number 1)(part_index 1))
	(Beam (index 21)(data continue)(number 1)(part_index 1))
	(Beam (index 22)(data begin)(number 2)(part_index 1))
	(Beam (index 23)(data end)(number 1)(part_index 1))
	(Beam (index 24)(data end)(number 2)(part_index 1))
	(Beam (index 25)(data begin)(number 1)(part_index 1))
	(Beam (index 26)(data continue)(number 1)(part_index 1))
	(Beam (index 27)(data begin)(number 2)(part_index 1))
	(Beam (index 28)(data end)(number 1)(part_index 1))
	(Beam (index 29)(data end)(number 2)(part_index 1))
	(Beam (index 30)(data begin)(number 1)(part_index 1))
	(Beam (index 31)(data end)(number 1)(part_index 1))
	(Beam (index 32)(data begin)(number 1)(part_index 1))
	(Beam (index 33)(data continue)(number 1)(part_index 1))
	(Beam (index 34)(data begin)(number 2)(part_index 1))
	(Beam (index 35)(data end)(number 1)(part_index 1))
	(Beam (index 36)(data end)(number 2)(part_index 1))
	(Beam (index 37)(data begin)(number 1)(part_index 1))
	(Beam (index 38)(data continue)(number 1)(part_index 1))
	(Beam (index 39)(data begin)(number 2)(part_index 1))
	(Beam (index 40)(data end)(number 1)(part_index 1))
	(Beam (index 41)(data end)(number 2)(part_index 1))
	(Beam (index 42)(data begin)(number 1)(part_index 1))
	(Beam (index 43)(data end)(number 1)(part_index 1))
	(Beam (index 44)(data begin)(number 1)(part_index 1))
	(Beam (index 45)(data continue)(number 1)(part_index 1))
	(Beam (index 46)(data begin)(number 2)(part_index 1))
	(Beam (index 47)(data end)(number 1)(part_index 1))
	(Beam (index 48)(data end)(number 2)(part_index 1))
	(Beam (index 49)(data begin)(number 1)(part_index 1))
	(Beam (index 50)(data continue)(number 1)(part_index 1))
	(Beam (index 51)(data begin)(number 2)(part_index 1))
	(Beam (index 52)(data end)(number 1)(part_index 1))
	(Beam (index 53)(data end)(number 2)(part_index 1))
	(Beam (index 54)(data begin)(number 1)(part_index 1))
	(Beam (index 55)(data end)(number 1)(part_index 1))
	(Beam (index 56)(data begin)(number 1)(part_index 1))
	(Beam (index 57)(data end)(number 1)(part_index 1))
	(Beam (index 58)(data begin)(number 1)(part_index 1))
	(Beam (index 59)(data end)(number 1)(part_index 1))
	(Beam (index 60)(data backward-hook)(number 2)(part_index 1))
	(Beam (index 61)(data begin)(number 1)(part_index 1))
	(Beam (index 62)(data end)(number 1)(part_index 1))
	(Beam (index 63)(data begin)(number 1)(part_index 1))
	(Beam (index 64)(data end)(number 1)(part_index 1))
	(Beam (index 65)(data begin)(number 1)(part_index 1))
	(Beam (index 66)(data end)(number 1)(part_index 1))
	(Beam (index 67)(data backward-hook)(number 2)(part_index 1))
)

(deffacts keys
	(Key (index 0)(fifths 0)(part_index 1))
)

(deffacts times
	(Time (index 0)(beats 2)(beat_type 4)(part_index 1))
)

(deffacts transposes
)

(deffacts directions
	(Direction (index 0)(placement above)(direction_type_index 0)(sound_index 0)(part_index 1))
)

(deffacts direction_types
	(Direction_Type (index 0)(metronome_index 0)(part_index 1))
)

(deffacts metronomes
	(Metronome (index 0)(parentheses no)(default_x -35.96)(relative_y 20.00)(beat_unit quarter)(per_minute 90)(part_index 1))
)

(deffacts sounds
	(Sound (index 0)(tempo 90)(part_index 1))
)

(deffacts clefs
	(Clef (index 0)(sign G)(line 2)(clef_octave_change -1)(part_index 1))
)

(deffacts barlines
	(Barline (index 0)(location right)(bar_style light-heavy)(part_index 1))
)

(deffacts ties
)

(deffacts instruments
)

(deffacts prints
)

(deffacts notationss
)

(deffacts backups
)

(deffacts unpitcheds
	(Unpitched (index 0)(part_index -1)(display_step A))
)

(deffacts articulationss
)

(deffacts repeats
)

(deffacts staff_layouts
)

(deffacts tieds
)

(deffacts system_layouts
)

(deffacts system_marginss
)

