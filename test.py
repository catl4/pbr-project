import numpy as np
from keras.models import load_model
from train import compute_song
from joblib import load


def music_test(file_name="test.txt", model=load_model('Model/network.h5')):
    inputs, l = compute_song(file_name)
    inputs = np.array(inputs)
    outputs = model.predict(inputs)
    sums = [0, 0, 0, 0, 0, 0]
    for output in outputs:
        sums = [s + a for s, a in zip(sums, output)]
    index = sums.index(max(sums))
    if index == 0:
        label = "Baroque"
    elif index == 1:
        label = "Classical"
    elif index == 2:
        label = "Folk"
    elif index == 3:
        label = "Jazz"
    elif index == 4:
        label = "Modern"
    else:
        label = "Renaissance"
    return label


if __name__ == '__main__':
    clf = load("Model/Random Forest.txt")
    print(music_test(model=clf))
