;(clear)
;(load "D:\\Facultate\\Anul III\\Sem 2\\PBR\\Proiect\\pbr-project\\resources\\Jazz\\Outputs\\output0.txt")
;(load "D:\\Facultate\\Anul III\\Sem 2\\PBR\\Proiect\\pbr-project\\resources\\Modern\\Outputs\\output0.txt")
;(load "D:\\Facultate\\Anul III\\Sem 2\\PBR\\Proiect\\pbr-project\\resources\\Renaissance\\Outputs\\output0.txt")

;(load "D:\\Facultate\\Anul III\\Sem 2\\PBR\\Proiect\\pbr-project\\rules.clp")
;(reset)
;(run)

; cris (load "D:\\School\\CLIPS\\pbr-project\\resources\\Jazz\\Outputs\\output0.txt")
; cris (load "D:\\School\\CLIPS\\pbr-project\\resources\\Modern\\Outputs\\output0.txt")
; cris (load "D:\\School\\CLIPS\\pbr-project\\resources\\Renaissance\\Outputs\\output0.txt")


(deftemplate part_statistics
    (slot part_index (type INTEGER))
    (slot no_measures (type INTEGER))
    (slot no_notes (type INTEGER))
    (slot pitched_A (type INTEGER))
    (slot pitched_B (type INTEGER))
    (slot pitched_C (type INTEGER))
    (slot pitched_D (type INTEGER))
    (slot pitched_E (type INTEGER))
    (slot pitched_F (type INTEGER))
    (slot pitched_G (type INTEGER))
    (slot unpitched_A (type INTEGER))
    (slot unpitched_B (type INTEGER))
    (slot unpitched_C (type INTEGER))
    (slot unpitched_D (type INTEGER))
    (slot unpitched_E (type INTEGER))
    (slot unpitched_F (type INTEGER))
    (slot unpitched_G (type INTEGER))
    (slot note_type_half (type INTEGER))
    (slot note_type_quarter (type INTEGER))
    (slot note_type_eighth (type INTEGER))
    (slot note_type_16th (type INTEGER))
    (slot stem_up (type INTEGER))
    (slot stem_down (type INTEGER))
)

(defrule export_statistics_to_file
    =>
	(open "D:\\School\\CLIPS\\pbr-project\\resources\\Modern\\Outputs_CLIPS\\statistics0.txt" data "w") ;cris
	;(open "D:\\School\\CLIPS\\pbr-project\\resources\\Modern\\Outputs_CLIPS\\statistics22.txt" data "w") ;cris
    ;(open "D:\\School\\CLIPS\\pbr-project\\resources\\Jazz\\Outputs_CLIPS\\statistics24.txt" data "w") ;cris
	(do-for-all-facts ((?f part_statistics)) TRUE
        (printout data "part_index " ?f:part_index crlf)
        (printout data "no_measures " ?f:no_measures crlf)
        (printout data "no_notes " ?f:no_notes crlf)
        (printout data "pitched_A " ?f:pitched_A crlf)
        (printout data "pitched_B " ?f:pitched_B crlf)
        (printout data "pitched_C " ?f:pitched_C crlf)
        (printout data "pitched_D " ?f:pitched_D crlf)
        (printout data "pitched_E " ?f:pitched_E crlf)
        (printout data "pitched_F " ?f:pitched_F crlf)
        (printout data "pitched_G " ?f:pitched_G crlf)
        (printout data "unpitched_A " ?f:unpitched_A crlf)
        (printout data "unpitched_B " ?f:unpitched_B crlf)
        (printout data "unpitched_C " ?f:unpitched_C crlf)
        (printout data "unpitched_D " ?f:unpitched_D crlf)
        (printout data "unpitched_E " ?f:unpitched_E crlf)
        (printout data "unpitched_F " ?f:unpitched_F crlf)
        (printout data "unpitched_G " ?f:unpitched_G crlf)
        (printout data "note_type_half " ?f:note_type_half crlf)
        (printout data "note_type_quarter " ?f:note_type_quarter crlf)
        (printout data "note_type_eighth " ?f:note_type_eighth crlf)
        (printout data "note_type_16th " ?f:note_type_16th crlf)
        (printout data "stem_up " ?f:stem_up crlf)
        (printout data "stem_down " ?f:stem_down crlf)
        (printout data "" crlf)
    )
    (close)
)


(defrule init_parts
    ?part <- (Part (part_index ?i))
    =>
    (assert (part_statistics (part_index ?i)))
)


(defrule note_count   
    ?n <- (Note (part_index ?i)(type ?type)(stem ?stem))
    ?part <- (part_statistics (part_index ?i)(no_notes ?notes) (stem_up ?su) (stem_down ?sd)
                              (note_type_half ?nh) (note_type_quarter ?nq) (note_type_eighth ?ne) (note_type_16th ?ns))
    =>
	(if (eq ?type half) then (modify ?part (note_type_half (+ ?nh 1))))
    (if (eq ?type quarter) then (modify ?part (note_type_quarter  (+ ?nq 1))))
    (if (eq ?type eighth) then (modify ?part (note_type_eighth  (+ ?ne 1))))
    (if (eq ?type 16th) then (modify ?part (note_type_16th  (+ ?ns 1))))
	(if (eq ?stem up) then (modify ?part (stem_up (+ ?su 1))))
    (if (eq ?stem down) then (modify ?part (stem_down  (+ ?sd 1))))
    (modify ?part (no_notes (+ ?notes 1)))
    (retract ?n)
)

(defrule measure_count
    ?m <- (Measure (part_index ?i))
    ?part <- (part_statistics (part_index ?i)(no_measures ?measures))
    =>
    (modify ?part (no_measures (+ ?measures 1)))
    (retract ?m)
)

(defrule is_pitched    
    ?p <- (Pitch (part_index ?i)(step ?step))
    ?part <- (part_statistics (part_index ?i)(pitched_A ?pa)(pitched_B ?pb)(pitched_C ?pc)(pitched_D ?pd)(pitched_E ?pe)(pitched_F ?pf)(pitched_G ?pg))
    =>
	(if (eq ?step A) then (modify ?part (pitched_A (+ ?pa 1))))
    (if (eq ?step B) then (modify ?part (pitched_B (+ ?pb 1))))
    (if (eq ?step C) then (modify ?part (pitched_C (+ ?pc 1))))
    (if (eq ?step D) then (modify ?part (pitched_D (+ ?pd 1))))
    (if (eq ?step E) then (modify ?part (pitched_E (+ ?pe 1))))
    (if (eq ?step F) then (modify ?part (pitched_F (+ ?pf 1))))
    (if (eq ?step G) then (modify ?part (pitched_G (+ ?pg 1))))
    (retract ?p)
)

(defrule is_unpitched    
    ?u <- (Unpitched (part_index ?i)(display_step ?dstep))
    ?part <- (part_statistics (part_index ?i)(unpitched_A ?ua)(unpitched_B ?ub)(unpitched_C ?uc)(unpitched_D ?ud)(unpitched_E ?ue)(unpitched_F ?uf)(unpitched_G ?ug))    
    =>
	(if (eq ?dstep A) then (modify ?part (unpitched_A (+ ?ua 1))))
    (if (eq ?dstep B) then (modify ?part (unpitched_B (+ ?ub 1))))
    (if (eq ?dstep C) then (modify ?part (unpitched_C (+ ?uc 1))))
    (if (eq ?dstep D) then (modify ?part (unpitched_D (+ ?ud 1))))
    (if (eq ?dstep E) then (modify ?part (unpitched_E (+ ?ue 1))))
    (if (eq ?dstep F) then (modify ?part (unpitched_F (+ ?uf 1))))
    (if (eq ?dstep G) then (modify ?part (unpitched_G (+ ?ug 1))))
    (retract ?u)
)

