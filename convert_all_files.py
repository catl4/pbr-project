from convertor import convert
import os

if __name__ == '__main__':
    for dirs in os.listdir("resources"):
        index = 0
        for file in os.listdir(os.path.join("resources", dirs)):
            convert(input_file="resources/" + str(os.path.join(dirs, file)).replace('\\', '/'),
                    output_file="resources/" + str(os.path.join(dirs, "Outputs/output{}.txt".format(index)).replace('\\', '/')))
            index += 1
            print(index)