(deftemplate counter    (slot name (type SYMBOL))    (slot number (type INTEGER)))
(defglobal ?*quarters* = 0)
(defglobal ?*halves* = 0)
(defglobal ?*eighths* = 0)
(defglobal ?*16ths* = 0)

(defglobal ?*max* = 0)

(deftemplate measure_per_parts (multislot measure (type INTEGER)))
(deffacts def_measure_per_parts
    (measure_per_parts)
)

(defrule count_per_part
    ?part <- (Part (measure ?a $? ?c))
    =>
    (bind ?*max* (+ ?c 1))
)

(defrule count_halves
    (declare (salience 10))

    ?note <- (Note (type ?t))
    (test (= (str-compare ?t "half") 0))
    =>
    (bind ?*halves* (+ ?*halves* 1))
)

(defrule increase_hcounter
    (declare (salience 9))

    ?counter <- (counter (name ?name)(number ?num))
    (test (= (str-compare ?name "half") 0))
    =>
    (modify ?counter (number ?*halves*) )
)

(defrule count_quarters
    (declare (salience 10))

    ?note <- (Note (type ?t))
    (test (= (str-compare ?t "quarter") 0))
    =>
    (bind ?*quarters* (+ ?*quarters* 1))
)

(defrule increase_qcounter
    (declare (salience 9))

    ?counter <- (counter (name ?name)(number ?num))
    (test (= (str-compare ?name "quarter") 0))
    =>
    (modify ?counter (number ?*quarters*) )
)

(defrule count_eighths
    (declare (salience 10))

    ?note <- (Note (type ?t))
    (test (= (str-compare ?t "eighth") 0))
    =>
    (bind ?*eighths* (+ ?*eighths* 1))
)

(defrule increase_8counter
    (declare (salience 9))

    ?counter <- (counter (name ?name)(number ?num))
    (test (= (str-compare ?name "eighth") 0))
    =>
    (modify ?counter (number ?*eighths*) )
)

(defrule count_16ths
    (declare (salience 10))

    ?note <- (Note (type ?t))
    (test (= (str-compare ?t "16th") 0))
    =>
    (bind ?*16ths* (+ ?*16ths* 1))
)

(defrule increase_16counter
    (declare (salience 9))

    ?counter <- (counter (name ?name)(number ?num))
    (test (= (str-compare ?name "16th") 0))
    =>
    (modify ?counter (number ?*16ths*) )
)


(deffacts counters
    (counter (name half)(number 0) )
    (counter (name quarter)(number 0) )
    (counter (name eighth)(number 0) )
    (counter (name 16th)(number 0) )
)
