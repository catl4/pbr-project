import re
import xml.sax.handler
from facts_classes import *
from copy import copy

Untracked_Tags = set()
Classes = [Part, Attributes, Measure, Note, Pitch, Beam, Key, Time, Transpose, Direction, Direction_Type, Metronome, Sound, Clef, Barline, Tie, Instrument, Print,
           Notations, Backup, Unpitched, Articulations, Repeat, Staff_layout, Tied, System_layout, System_margins]


class DataNode(object):
    def __init__(self):
        self._attrs = {}  # XML attributes and child elements
        self.data = None  # child text data

    def __len__(self):
        # treat single element as a list of 1
        return 1

    def __getitem__(self, key):
        if isinstance(key, str):
            return self._attrs.get(key, None)
        else:
            return [self][key]

    def __contains__(self, name):
        return self._attrs.has_key(name)

    def __nonzero__(self):
        return bool(self._attrs or self.data)

    def __getattr__(self, name):
        if name.startswith('__'):
            # need to do this for Python special methods???
            raise AttributeError(name)
        return self._attrs.get(name, None)

    def _add_xml_attr(self, name, value):
        if name in self._attrs:
            # multiple attribute of the same name are represented by a list
            children = self._attrs[name]
            if not isinstance(children, list):
                children = [children]
                self._attrs[name] = children
            children.append(value)
        else:
            self._attrs[name] = value

    def __str__(self):
        return self.data or ''

    def __repr__(self):
        items = sorted(self._attrs.items())
        if self.data:
            items.append(('data', self.data))
        return u'{%s}' % ', '.join([u'%s:%s' % (k, repr(v)) for k, v in items])


def xml2obj(src):
    """
    A simple function to converts XML data into native Python object.
    """

    non_id_char = re.compile('[^_0-9a-zA-Z]')

    def _name_mangle(name):
        return non_id_char.sub('_', name)

    class TreeBuilder(xml.sax.handler.ContentHandler):
        def __init__(self):
            self.stack = []
            self.root = DataNode()
            self.current = self.root
            self.text_parts = []

        def startElement(self, name, attrs):
            self.stack.append((self.current, self.text_parts))
            self.current = DataNode()
            self.text_parts = []
            # xml attributes --> python attributes
            for k, v in attrs.items():
                self.current._add_xml_attr(_name_mangle(k), v)

        def endElement(self, name):
            text = ''.join(self.text_parts).strip()
            if text:
                self.current.data = text
            if self.current._attrs:
                obj = self.current
            else:
                # a text only node is simply represented by the string
                obj = text or ''
            self.current, self.text_parts = self.stack.pop()
            self.current._add_xml_attr(_name_mangle(name), obj)

        def characters(self, content):
            self.text_parts.append(content)

    builder = TreeBuilder()
    try:
        if isinstance(src, str):
            xml.sax.parseString(src, builder)
        else:
            xml.sax.parse(src, builder)
    except:
        pass
    return builder.root._attrs


def process_list(List, class_name, part_index=False):
    indexes = list()
    try:
        assert isinstance(List[0], DataNode)
        for elem in List:
            indexes.append(process_node(elem, class_name, part_index))
    except:
        for elem in List:
            indexes.append(elem)
    return indexes


def process_node(Node, class_name, part_index=False):
    if class_name == 'part':
        part_index = Node._attrs['id'].replace('P', '')
    node = 0
    if class_name == 'part':
        node = Part()
    elif class_name == 'attributes':
        node = Attributes()
    elif class_name == 'measure':
        node = Measure()
    elif class_name == 'note':
        node = Note()
    elif class_name == 'pitch':
        node = Pitch()
    elif class_name == 'beam':
        node = Beam()
    elif class_name == 'key':
        node = Key()
    elif class_name == 'time':
        node = Time()
    elif class_name == 'transpose':
        node = Transpose()
    elif class_name == 'direction':
        node = Direction()
    elif class_name == 'direction_type':
        node = Direction_Type()
    elif class_name == 'metronome':
        node = Metronome()
    elif class_name == 'sound':
        node = Sound()
    elif class_name == 'clef':
        node = Clef()
    elif class_name == 'barline':
        node = Barline()
    elif class_name == 'tie':
        node = Tie()
    elif class_name == 'instrument':
        node = Instrument()
    # elif class_name == 'print':
    #     node = Print()
    elif class_name == 'notations':
        node = Notations()
    # elif class_name == 'backup':
    #     node = Backup()
    elif class_name == 'unpitched':
        node = Unpitched()
    elif class_name == 'articulations':
        node = Articulations()
    elif class_name == 'repeat':
        node = Repeat()
    elif class_name == 'staff_layout':
        node = Staff_layout()
    elif class_name == 'tied':
        node = Tied()
    elif class_name == 'credit':
        node = Credit()
    elif class_name == 'credit_words':
        node = CreditWords()
    # elif class_name == 'system_layout':
    #     node = System_layout()
    # elif class_name == 'system_margins':
    #     node = System_margins()
    elif class_name == 'lyric':
        node = Lyric()
    else:
        Untracked_Tags.add(class_name)
        return ""

    if Node.data != None:
        if isinstance(Node.data, str) and ' ' in Node.data:
            Node.data = copy(Node.data.replace(' ', '-'))
        node.dictionary['data'] = Node.data

    for key, value in Node._attrs.items():
        if isinstance(value, DataNode):
            index = process_node(Node=value, class_name=key, part_index=part_index)
            if not isinstance(index, str):
                node.dictionary[key + "_index"] = index
        elif isinstance(value, list):
            node.dictionary[key] = process_list(List=value, class_name=key, part_index=part_index)
        else:
            if value:
                node.dictionary[key] = value
    node.lista.append(node)
    node.dictionary['part_index'] = part_index

    return node.index


def process_xml(xml_string):
    dictionary = xml2obj(xml_string)
    score_partwise = dictionary['score_partwise']

    for key, value in score_partwise._attrs.items():
        if isinstance(score_partwise[key], list) or isinstance(score_partwise[key], DataNode):
            if isinstance(score_partwise[key], list):
                process_list(score_partwise[key], key)
            else:
                process_node(score_partwise[key], key)
                # print("Untracked Tags: ", Untracked_Tags)
    if len(Unpitched.lista) == 0:
        node = Unpitched()
        node.dictionary['part_index'] = -1
        node.dictionary['display_step'] = "A"
        Unpitched.lista.append(node)


def extract_facts_templates(output_file="output.txt"):
    fd2 = open("templates.txt", 'w')
    fd = open("facts.txt", 'w')

    for Class in Classes:
        string = "(deffacts {}s\n".format(Class.name.lower())
        attributes_slot = set()
        attributes_multislot = set()
        for instance in Class.lista:
            string += "\t({} ".format(Class.name)
            for key, value in instance.dictionary.items():
                if not isinstance(value, list):
                    string += "({} {})".format(key, value)
                    attributes_slot.add(key)
                else:
                    string += "({}".format(key)
                    for item in value:
                        string += " {}".format(item)
                    string += ")"
                    attributes_multislot.add(key)
            string += ')\n'
        string += ")\n\n"
        fd.write(string)

        string = "(deftemplate {}\n".format(Class.name)
        aux = set()
        for attribute in attributes_slot:
            if attribute not in attributes_multislot:
                aux.add(attribute)
        attributes_slot = copy(aux)
        for attribute in attributes_slot:
            string += "\t(slot {})\n".format(attribute)
        for attribute in attributes_multislot:
            string += "\t(multislot {})\n".format(attribute)
        string += ")\n\n"
        fd2.write(string)

    fd.close()
    fd2.close()
    fd2 = open("facts.txt", 'r')
    fd = open("templates.txt", 'r')
    fd3 = open(output_file, 'w')
    fd3.write(fd.read())
    fd3.write(fd2.read())
    fd.close()
    fd2.close()
    fd3.close()


def convert(input_file, output_file):
    for Class in Classes:
        Class.lista = list()
        Class.nr = 0
    try:
        fd = open(input_file, 'r')
        process_xml(fd.read())
        extract_facts_templates(output_file)
        fd.close()
    except:
        pass


if __name__ == '__main__':
    convert(input_file="resources/We_Will_Rock_You/lg-69623657.xml", output_file="output.txt")
