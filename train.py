import os
import numpy as np
from random import random as rnd
from keras.layers import Dropout, Dense
from keras.models import load_model
import keras
from sklearn.metrics import f1_score, accuracy_score, recall_score
from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.ensemble import GradientBoostingClassifier
from joblib import dump


def compute_song(file_name, label=0):
    statistics = open(file_name, 'r').read()
    Xs = list()
    Ys = list()
    for statistic in statistics.split('\n\n'):
        if statistic != '':
            if label == 'Baroque':
                y = [1, 0, 0, 0, 0, 0]
            elif label == "Classical":
                y = [0, 1, 0, 0, 0, 0]
            elif label == "Folk":
                y = [0, 0, 1, 0, 0, 0]
            elif label == "Jazz":
                y = [0, 0, 0, 1, 0, 0]
            elif label == "Modern":
                y = [0, 0, 0, 0, 1, 0]
            else:
                y = [0, 0, 0, 0, 0, 1]
            Ys.append(np.array(y))
            X = list()
            # for field in statistic.split('\n')[1:10] + statistic.split('\n')[17:]:
            for field in statistic.split('\n')[1:]:
                X.append(float(field.split(' ')[1]))
            Xs.append(np.array(X))
    return Xs, Ys


def evaluate_neural_network(test_data, test_labels, model=load_model('Model/network.h5')):
    y_true = list()
    y_pred = list()
    for data, label in zip(test_data, test_labels):
        predicted_label = model.predict(np.array([data]))
        if isinstance(predicted_label[0], np.int64):
            y_pred.append(predicted_label[0])
        else:
            y_pred.append(np.where(predicted_label[0] == max(predicted_label[0]))[0])

        y_true.append(np.where(label == max(label))[0])
    f1 = f1_score(y_true, y_pred, average=None)
    acc = accuracy_score(y_true, y_pred)
    recall = recall_score(y_true, y_pred, average=None)
    return f1, acc, recall


def initialize_model(input_size):
    model = keras.models.Sequential()
    model.add(Dense(input_size, activation="sigmoid"))
    model.add(Dropout(0.25))
    model.add(Dense(10, activation="sigmoid"))
    model.add(Dense(6, activation="softmax"))
    model.compile(optimizer=keras.optimizers.Adam(lr=0.01), loss="categorical_crossentropy", metrics=["accuracy"])
    return model


def get_data():
    training_data = list()
    testing_data = list()
    training_labels = list()
    testing_labels = list()
    for dirs in os.listdir("resources"):
        path = str(os.path.join("resources", dirs)).replace('\\', '/') + "/Outputs_CLIPS"
        for file in os.listdir(path):
            Xs, Ys = compute_song(file_name=str(os.path.join(path, file)).replace('\\', '/'), label=dirs)
            for X, y in zip(Xs, Ys):
                if 0.8 > rnd():
                    training_data.append(np.array(X))
                    training_labels.append(np.array(y))
                else:
                    testing_data.append(np.array(X))
                    testing_labels.append(np.array(y))
    return np.array(training_data), np.array(training_labels), np.array(testing_data), np.array(testing_labels)


def classify(X_train, y_train, X_test, y_test):
    y_train = [np.where(y == max(y))[0][0] for y in y_train]
    y_test = [np.where(y == max(y))[0][0] for y in y_test]

    names = ["Nearest Neighbors",
             "Linear SVM",
             "RBF SVM",
             "Decision Tree",
             "Random Forest",
             "Neural Net",
             "AdaBoost",
             "Naive Bayes",
             "SGD",
             "Gradient Boosting"]

    classifiers = [
        KNeighborsClassifier(3),
        SVC(kernel="linear", C=0.025),
        SVC(gamma=2, C=1),
        DecisionTreeClassifier(),
        RandomForestClassifier(),
        MLPClassifier(alpha=0.1, max_iter=100),
        AdaBoostClassifier(),
        GaussianNB(),
        SGDClassifier(),
        GradientBoostingClassifier(learning_rate=0.1, random_state=0, loss='ls')]

    best_clf = classifiers[0]
    best_score = 0
    best_clf_name = 0
    # iterate over classifiers
    for name, clf in zip(names, classifiers):
        try:
            clf.fit(X_train, y_train)
            score = clf.score(X_test, y_test)
            if score > best_score:
                best_score, best_clf, best_clf_name = score, clf, name
                # print("{}:  {}".format(name, score))
        except:
            pass
    return best_clf, best_clf_name


if __name__ == '__main__':
    training_data, training_labels, testing_data, testing_labels = get_data()

    acc = 0
    while acc < 0.69:
        clf, name = classify(training_data, training_labels, testing_data, testing_labels)
        f1, acc, recall = evaluate_neural_network(testing_data, testing_labels, clf)
    print("{}: ".format(name))
    print("F1: ", f1)
    print("Acc: ", acc)
    print("Recall: ", recall)
    dump(clf, "Model/{}.txt".format(name))

    acc = 0
    trial = 0
    while acc < 0.41:
        model = initialize_model(input_size=len(training_data[0]))
        model.fit(x=training_data, y=training_labels, epochs=max(10, trial), batch_size=20, verbose=False)
        loss, acc = model.evaluate(testing_data, testing_labels, verbose=False)
        # print("Trial: {}".format(trial), "Accuracy: {}".format(acc))
        trial += 1

    model.save('Model/network.h5')
    print("\nNeural Network: ")
    f1, acc, recall = evaluate_neural_network(testing_data, testing_labels, model)
    print("F1: ", f1)
    print("Acc: ", acc)
    print("Recall: ", recall)
