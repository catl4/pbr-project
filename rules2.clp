(deftemplate note_type_counter (slot name (type SYMBOL))(slot number (type INTEGER)) )

(defrule export_to_file_statistics
    =>
    (open "D:\\School\\CLIPS\\pbr-project\\statistics.txt" mydata "w")
    (do-for-all-facts ((?f note_type_counter)) TRUE
        (format mydata "%s : %d%n" ?f:name ?f:number)
    )
    (close)
)

(defrule f_rule
    ?f <- (note_type_counter)
    =>
)

(defrule count_notes_by_type
    ?note <- (Note (type ?t))
    ?counter <- (note_type_counter (name ?name)(number ?num))
    (test (= (str-compare ?t ?name) 0))
    =>
    (modify ?counter (number (+ ?num 1) ) )
    (retract ?note)
)

(deffacts note_type_counters
    (note_type_counter (name half)(number 0) )
    (note_type_counter (name quarter)(number 0) )
    (note_type_counter (name eighth)(number 0) )
    (note_type_counter (name 16th)(number 0) )
)

(deftemplate pitched_note_counter (slot name (type SYMBOL))(slot number (type INTEGER)) )

(defrule count_pitched_note
    ?pitch <- (Pitch (step ?s))
    ?counter <- (pitched_note_counter (name ?name)(number ?num))
    (test (= (str-compare ?s ?name) 0))
    =>
    (modify ?counter (number (+ ?num 1) ) )
    (retract ?pitch)
)

(deffacts pitched_note_counters
    (pitched_note_counter (name A)(number 0) )
    (pitched_note_counter (name B)(number 0) )
    (pitched_note_counter (name C)(number 0) )
    (pitched_note_counter (name D)(number 0) )
	(pitched_note_counter (name E)(number 0) )
	(pitched_note_counter (name F)(number 0) )
	(pitched_note_counter (name G)(number 0) )
)

(deftemplate unpitched_note_counter (slot name (type SYMBOL))(slot number (type INTEGER)) )

(defrule count_unpitched_note
    ?unpitched <- (Unpitched (display_step ?d))
    ?counter <- (unpitched_note_counter (name ?name)(number ?num))
    (test (= (str-compare ?d ?name) 0))
    =>
    (modify ?counter (number (+ ?num 1) ) )
    (retract ?unpitched)
)

(deffacts unpitched_note_counters
    (unpitched_note_counter (name A)(number 0) )
    (unpitched_note_counter (name B)(number 0) )
    (unpitched_note_counter (name C)(number 0) )
    (unpitched_note_counter (name D)(number 0) )
	(unpitched_note_counter (name E)(number 0) )
	(unpitched_note_counter (name F)(number 0) )
	(unpitched_note_counter (name G)(number 0) )
)