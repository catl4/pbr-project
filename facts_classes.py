class Barline:
    nr = 0
    lista = list()
    name = "Barline"

    def __init__(self):
        self.dictionary = dict()
        self.index = Barline.nr
        self.dictionary["index"] = self.index

        Barline.nr += 1

class Credit:
    nr = 0
    lista = list()
    name = "Credit"

    def __init__(self):
        self.dictionary = dict()
        self.index = Credit.nr
        self.dictionary["index"] = self.index

        Credit.nr += 1

class CreditWords:
    nr = 0
    lista = list()
    name = "CreditWords"

    def __init__(self):
        self.dictionary = dict()
        self.index = CreditWords.nr
        self.dictionary["index"] = self.index

        CreditWords.nr += 1


class Lyric:
    nr = 0
    lista = list()
    name = "Lyric"

    def __init__(self):
        self.dictionary = dict()
        self.index = Lyric.nr
        self.dictionary["index"] = self.index

        Lyric.nr += 1


class Articulations:
    nr = 0
    lista = list()
    name = "Articulations"

    def __init__(self):
        self.dictionary = dict()
        self.index = Articulations.nr
        self.dictionary["index"] = self.index

        Articulations.nr += 1


class Repeat:
    nr = 0
    lista = list()
    name = "Repeat"

    def __init__(self):
        self.dictionary = dict()
        self.index = Repeat.nr
        self.dictionary["index"] = self.index

        Repeat.nr += 1


class Staff_layout:
    nr = 0
    lista = list()
    name = "Staff_layout"

    def __init__(self):
        self.dictionary = dict()
        self.index = Staff_layout.nr
        self.dictionary["index"] = self.index

        Staff_layout.nr += 1


class Tied:
    nr = 0
    lista = list()
    name = "Tied"

    def __init__(self):
        self.dictionary = dict()
        self.index = Tied.nr
        self.dictionary["index"] = self.index

        Tied.nr += 1


class System_layout:
    nr = 0
    lista = list()
    name = "System_layout"

    def __init__(self):
        self.dictionary = dict()
        self.index = System_layout.nr
        self.dictionary["index"] = self.index

        System_layout.nr += 1


class System_margins:
    nr = 0
    lista = list()
    name = "System_margins"

    def __init__(self):
        self.dictionary = dict()
        self.index = System_margins.nr
        self.dictionary["index"] = self.index

        System_margins.nr += 1


class Tie:
    nr = 0
    lista = list()
    name = "Tie"

    def __init__(self):
        self.dictionary = dict()
        self.index = Tie.nr
        self.dictionary["index"] = self.index

        Tie.nr += 1


class Instrument:
    nr = 0
    lista = list()
    name = "Instrument"

    def __init__(self):
        self.dictionary = dict()
        self.index = Instrument.nr
        self.dictionary["index"] = self.index

        Instrument.nr += 1


class Print:
    nr = 0
    lista = list()
    name = "Print"

    def __init__(self):
        self.dictionary = dict()
        self.index = Print.nr
        self.dictionary["index"] = self.index

        Print.nr += 1


class Notations:
    nr = 0
    lista = list()
    name = "Notations"

    def __init__(self):
        self.dictionary = dict()
        self.index = Notations.nr
        self.dictionary["index"] = self.index

        Notations.nr += 1


class Backup:
    nr = 0
    lista = list()
    name = "Backup"

    def __init__(self):
        self.dictionary = dict()
        self.index = Backup.nr
        self.dictionary["index"] = self.index

        Backup.nr += 1


class Unpitched:
    nr = 0
    lista = list()
    name = "Unpitched"
    def __init__(self):
        self.dictionary = dict()
        self.index = Unpitched.nr
        self.dictionary["index"] = self.index

        Unpitched.nr += 1


class Sound:
    nr = 0
    lista = list()
    name = "Sound"

    def __init__(self):
        self.dictionary = dict()
        self.index = Sound.nr
        self.dictionary["index"] = self.index

        Sound.nr += 1


class Metronome:
    nr = 0
    lista = list()
    name = "Metronome"

    def __init__(self):
        self.dictionary = dict()
        self.index = Metronome.nr
        self.dictionary["index"] = self.index

        Metronome.nr += 1


class Direction_Type:
    nr = 0
    lista = list()
    name = "Direction_Type"

    def __init__(self):
        self.dictionary = dict()
        self.index = Direction_Type.nr
        self.dictionary["index"] = self.index

        Direction_Type.nr += 1


class Direction:
    nr = 0
    lista = list()
    name = "Direction"

    def __init__(self):
        self.dictionary = dict()
        self.index = Direction.nr
        self.dictionary["index"] = self.index

        Direction.nr += 1


class Transpose:
    nr = 0
    lista = list()
    name = "Transpose"

    def __init__(self):
        self.dictionary = dict()
        self.index = Transpose.nr
        self.dictionary["index"] = self.index
        Transpose.lista = list()
        Transpose.nr += 1


class Time:
    nr = 0
    lista = list()
    name = "Time"

    def __init__(self):
        self.dictionary = dict()
        self.index = Time.nr
        self.dictionary["index"] = self.index

        Time.nr += 1


class Key:
    nr = 0
    lista = list()
    name = "Key"

    def __init__(self):
        self.dictionary = dict()
        self.index = Key.nr
        self.dictionary["index"] = self.index

        Key.nr += 1


class Clef:
    nr = 0
    lista = list()
    name = "Clef"

    def __init__(self):
        self.dictionary = dict()
        self.index = Clef.nr
        self.dictionary["index"] = self.index

        Clef.nr += 1


class Attributes:
    nr = 0
    lista = list()
    name = "Attributes"

    def __init__(self):
        self.dictionary = dict()
        self.index = Attributes.nr
        self.dictionary["index"] = self.index

        Attributes.nr += 1


class Beam:
    nr = 0
    lista = list()
    name = "Beam"

    def __init__(self):
        self.dictionary = dict()
        self.index = Beam.nr
        self.dictionary["index"] = self.index

        Beam.nr += 1


class Pitch:
    nr = 0
    lista = list()
    name = "Pitch"

    def __init__(self):
        self.dictionary = dict()
        self.index = Pitch.nr
        self.dictionary["index"] = self.index

        Pitch.nr += 1


class Note:
    nr = 0
    lista = list()
    name = "Note"

    def __init__(self):
        self.dictionary = dict()
        self.index = Note.nr
        self.dictionary["index"] = self.index

        Note.nr += 1


class Measure:
    nr = 0
    lista = list()
    name = "Measure"

    def __init__(self):
        self.dictionary = dict()
        self.index = Measure.nr
        self.dictionary["index"] = self.index

        Measure.nr += 1


class Part:
    nr = 0
    lista = list()
    name = "Part"

    def __init__(self):
        self.dictionary = dict()
        self.index = Part.nr
        self.dictionary["index"] = self.index

        Part.nr += 1

